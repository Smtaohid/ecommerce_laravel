<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('category_name')->nullable();
            $table->string('slug')->nullable();
            $table->integer('slug_id')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->integer('order_by')->nullable();
            $table->tinyInteger('primary_menu')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('photo')->nullable();
            $table->string('banner')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
