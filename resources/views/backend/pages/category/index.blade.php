@extends('backend.layouts.master')
@section('page_title', 'Category List')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h2>Category List</h2>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover $table-striped ">
                    <thead>
                        <tr>
                            <th>SL</th>
                            <th>Category</th>
                            <th>Order By</th>
                            <th>Primay Menu</th>
                            <th>Banner</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $sl = 1;
                        @endphp
                        @foreach ($categories as $category)
                        <tr>
                            <td>{{ $sl++ }}</td>
                            <td>
                                <div class="d-flex align-items-center justify-content-center">
                                    <img src="{{asset('images/uploads/category/'.$category->photo)}}"
                                        alt="{{$category->category_name}}" class="table-image" />
                                    {{$category->category_name}}

                                </div>
                            </td>

                            <td>{{ $category->order_by }}</td>
                            <td class="align-item-center align-middel justify-content-center">

                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" {{ $category->primary_menu == 1 ?
                                    'checked': 'null' }} >
                                </div>
                            </td>
                            <td>
                                <img src="{{asset('images/uploads/category/'.$category->banner)}}"
                                    class="table-image-banner" alt="{{ $category->category_name }}" />
                            </td>
                            <td>
                                <div class="d-flex justify-content-center">
                                    <a href="{{ route('category.show',$category->id) }}" class="btn btn-outline-success btn-sm mx-1"><i class="fas fa-eye "></i></a>
                                    <a href="{{ route('category.edit',$category->id) }}" class="btn btn-outline-warning btn-sm mx-1"><i class="fas fa-edit "></i></a>
                                    {!! Form::open(['method'=>'delete','route'=>['category.destroy',$category->id]]) !!}
                                    {!! Form::button('<i class="fas fa-trash "></i>', ['type'=>'submit','class'=>'btn btn-outline-danger btn-sm mx-1']) !!}
                                    {!! Form::close() !!}

                                </div>

                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
