@extends('backend.layouts.master')
@section('page_title', 'Category details')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card">
            <div class="card-header">
                <h2>Category details</h2>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover $table-striped ">
                    <tr>
                        <th>ID</th>
                        <td>{{ $category->id }}</td>
                    </tr>
                    <tr>
                        <th>CATEGORY NAME</th>
                        <td>{{ $category->category_name}}</td>
                    </tr>
                    <tr>
                        <th>SLUG</th>
                        <td>{{ $category->slug}}</td>
                    </tr>
                    <tr>
                        <th>SLUG ID</th>
                        <td>{{ $category->slug_id}}</td>
                    </tr>
                    <tr>
                        <th>STATUS</th>
                        <td>{{ $category->status == 1 ? 'Active':"Inactive"}}</td>
                    </tr>
                    <tr>
                        <th>DESCRIPTION</th>
                        <td>{{ $category->description}}</td>
                    </tr>
                    <tr>
                        <th>KEYWODS</th>
                        <td>{{ $category->keywords}}</td>
                    </tr>
                    <tr>
                        <th>ORDERBY</th>
                        <td>{{ $category->order_by}}</td>
                    </tr>
                    <tr>
                        <th>PRIMARY MENU</th>
                        <td>{{ $category->primary_menu == 1 ? 'Yes':'No'}}</td>
                    </tr>
                    <tr>
                        <th>CREATED AT</th>
                        <td>{{ $category->created_at->toDayDateTimeString()}}</td>
                    </tr>
                    <tr>
                        <th>UPdATED AT</th>
                        <td>{{$category->updated_at != $category->created_at ?
                            $category->updated_at->toDayDateTimeString():'Not Update'}}</td>
                    </tr>
                </table>
                <div class="row my-4">
                    <div class="col-md-12">
                        <p><span  class="lead"> Child Category Of <span class="btn btn-outline-success btn-sm fw-bold">{{ $category->category_name }}</span></p>
                        <ul>
                            @foreach ($category->childCategory as $sub_category )


                            <li>{{ $sub_category->category_name }}
                                <ul>
                                    @foreach ($sub_category->childCategory as $subSub_category )
                                    <li>
                                        {{ $subSub_category->category_name }}
                                        <ul>
                                            @foreach ($subSub_category->childCategory as $subSubSub_category )
                                            <li>
                                                {{ $subSubSub_category->category_name }}
                                                <ul>
                                                    @foreach ($subSubSub_category->childCategory as $subSubSubSub_category )
                                                    <li>
                                                        {{ $subSubSubSub_category->category_name }}
                                                        <ul>
                                                            @foreach ($subSubSubSub_category->childCategory as $subSubSubSubSub_category )
                                                            <li>
                                                                {{ $subSubSubSubSub_category->category_name }}
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p>Category Photo</p>
                        <div class="downloadable-image">
                            <img src="{{ asset('images/uploads/category/'.$category->photo) }}" class="img-thumbnail"
                                alt="{{ $category->category_name }}">
                            <div class="download-function-area">
                                <div class="download-content">
                                    <a href="{{ url('images/uploads/category/'.$category->photo) }}"
                                        target="_blank"><button class="btn btn-success btn-sm"><i
                                                class="fas fa-eye"></i></button></a>

                                    <button class="btn btn-info btn-sm"><i
                                            class="fas fa-cloud-download-alt"></i></button>
                                </div>

                            </div>
                        </div>


                    </div>
                    <div class="col-md-6">
                        <p>Category Banner</p>

                        <a href="{{ url('images/uploads/category/'.$category->banner) }}" target="_blank"
                            rel="noopener noreferrer"><img
                                src="{{ asset('images/uploads/category/'.$category->banner) }}"
                                class="img-thumbnail mt-2" alt="{{ $category->category_name }}"></a>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
