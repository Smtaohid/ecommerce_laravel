@extends('backend.layouts.master')
@section('page_title', 'Update Category')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <h2>Update Category</h2>
                </div>
                <div class="card-body">
                    {!! Form::model($category,['method' => 'put', 'files' => true, 'route' => ['category.update',$category->id]]) !!}
                     @include('backend.pages.category.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
