@extends('backend.layouts.master')
@section('page_title', 'Category Create')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">
                    <h2>Create New Category</h2>
                </div>
                <div class="card-body">
                    {!! Form::open(['method' => 'post', 'files' => true, 'route' => 'category.store']) !!}
                     @include('backend.pages.category.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
