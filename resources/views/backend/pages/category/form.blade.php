<div class="row align-items-center">
    <div class="col-md-3">
        {!! Form::label('name', 'Category Name : ') !!}
    </div>
    <div class="col-md-9">
        {!! Form::text('category_name', null, ['id' => 'category_name','class' => 'form-control' . ($errors->has('category_name') ? ' is-invalid' : null), 'placeholder' => 'Category Name']) !!}
        @error('category_name')
            <p class="error-message">{{ $message }}</p>
        @enderror
    </div>
</div>
<div class="row d-flex align-items-center mt-4">
    <div class="col-md-3">
        {!! Form::label('slug', 'Category Slug  : ') !!}
    </div>
    <div class="col-md-9">
        {!! Form::text('slug', null, ['id' => 'slug', 'class' => 'form-control' . ($errors->has('slug') ? ' is-invalid' : null), 'placeholder' => 'Category Slug ']) !!}
        @error('slug')
            <p class="error-message">{{ $message }}</p>
        @enderror
    </div>
</div>

<div class="row d-flex align-items-center mt-4">
    <div class="col-md-3">
        {!! Form::label('status', 'Select Status  : ') !!}
    </div>
    <div class="col-md-9">
        {!! Form::select('status', [0 => 'Inactive', 1 => 'Active'], null, ['class' => 'form-control' . ($errors->has('status') ? ' is-invalid' : null), 'placeholder' => 'Select Status ']) !!}
        @error('status')
            <p class="error-message">{{ $message }}</p>
        @enderror
    </div>
</div>

<div class="row d-flex align-items-center mt-4">
    <div class="col-md-3">
        {!! Form::label('description', 'Description : ') !!}
    </div>
    <div class="col-md-9">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description ', 'rows' => 4]) !!}
    </div>
</div>
<div class="row d-flex align-items-center mt-4">
    <div class="col-md-3">
        {!! Form::label('keywords', 'Keywords : ') !!}
    </div>
    <div class="col-md-9">
        {!! Form::text('keywords', null, ['class' => 'form-control', 'placeholder' => 'Keywords ']) !!}
    </div>
</div>
<div class="row d-flex align-items-center mt-4">
    <div class="col-md-3">
        {!! Form::label('order_by', 'Order By : ') !!}

    </div>
    <div class="col-md-9">
        {!! Form::text('order_by', null, ['class' => 'form-control' . ($errors->has('order_by') ? ' is-invalid' : null), 'placeholder' => 'Order By ']) !!}
        @error('order_by')
            <p class="error-message">{{ $message }}</p>
        @enderror
    </div>
</div>
<div class="row d-flex align-items-center justify-content-between mt-4">
    <div class="col-md-3">
        {!! Form::label('primary_menu', 'Primary Menu : ') !!}
    </div>
    <div class="col-md-9">
        {!! Form::checkbox('primary_menu', 1 ,  false) !!}
    </div>
</div>

<div class="row d-flex align-items-center mt-4">
    <div class="col-md-3">
        {!! Form::label('category_id', 'Select Category : ') !!}
    </div>
    <div class="col-md-9">
        {!! Form::select('category_id', $categories, null,['class' => 'form-control', 'placeholder' => 'Select Category ']) !!}
    </div>
</div>

<div class="row d-flex align-items-center mt-4">
    <div class="col-md-3">
        {!! Form::label('photo', 'Photo : ') !!}
    </div>
    <div class="col-md-9">
        {!! Form::file('photo', null, ['class' => 'form-control form-control-sm', 'placeholder' => 'photo ']) !!}
    </div>
</div>
<div class="row d-flex align-items-center mt-4">
    <div class="col-md-3">
        {!! Form::label('banner', 'Banner : ') !!}
    </div>
    <div class="col-md-9">
        {!! Form::file('banner', null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Banner ']) !!}
    </div>
</div>
<div class="row d-flex align-items-center mt-4">
    <button class="btn btn-outline-success form-control" type="submit">{{ Route::current()->getName() == 'category.edit'? 'Update':'Create' }}</button>
</div>
@push('script')
    <script>
        $('#category_name').on('input', function() {
            let value = $(this).val()
            value = value.replaceAll(' ', '-')
            $('#slug').val(value)
        })
    </script>
@endpush
