@extends('frontend.layouts.master')
@section('content')
@include('frontend.includs.home.banner')
@include('frontend.includs.home.banner_bottom')
<!-- schedule-bottom -->
@include('frontend.includs.home.banner_schedule')
<!-- //schedule-bottom -->
<!-- banner-bootom-w3-agileits -->
@include('frontend.includs.home.banner_trending')
<!--/grids-->
@include('frontend.includs.home.offer')
<!--/grids-->
<!-- /new_arrivals -->
@include('frontend.includs.home.new_arrivals')
<!-- //new_arrivals -->
<!-- /we-offer -->
@include('frontend.includs.home.discount')
@endsection
