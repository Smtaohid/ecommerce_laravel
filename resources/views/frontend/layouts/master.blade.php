
<!DOCTYPE html>
<html>
<head>
   @include('frontend.includs.head')
</head>
<body>
<!-- header -->
<div class="header" id="home">
@include('frontend.includs.header')
</div>
<!-- //header -->
<!-- header-bot -->
<div class="header-bot">
    @include('frontend.includs.header_bot')
</div>
<!-- //header-bot -->
<!-- banner -->
<div class="ban-top">
@include('frontend.includs.menu')
</div>
<!-- //banner-top -->
<!-- Modal1 -->
@include('frontend.includs.signin_modal')
<!-- //Modal1 -->
<!-- Modal2 -->
@include('frontend.includs.singup_molda')
<!-- //Modal2 -->
{{-- content --}}
@yield('content')
{{-- /content --}}
<!-- banner -->

<!-- //we-offer -->
<!--/grids-->
@include('frontend.includs.coupon')
<!--grids-->
<!-- footer -->
@include('frontend.includs.footer')
<!-- //footer -->

<!-- login -->
@include('frontend.includs.footer_login')
<!-- //login -->
{{-- script --}}
    @include('frontend.includs.seript')
{{-- /script --}}
</body>
</html>
