<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;




class ImageUploadController extends Controller
{
   public static function uploadImage($file, $width, $height, $name, $path)
   {
    $image_file_name = $name. '.webp';
    Image::make($file)->fit($width, $height)->save(public_path($path).$image_file_name, 50, 'webp');
    return $image_file_name;
   }
    /**
     * @var mixed path
     */
   public static function unlinkImage($path ,$img)
   {


    $path = public_path($path).$img;
    if ($img !== '' && file_exists($path)) {
      unlink($path);
    }
   }
}
