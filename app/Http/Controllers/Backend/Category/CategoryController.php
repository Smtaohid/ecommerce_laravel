<?php

namespace App\Http\Controllers\Backend\Category;

use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageUploadController;
use App\Http\Requests\CategoryRequest;
use Symfony\Component\CssSelector\Node\ElementNode;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with(['childCategory'])->whereNull('category_id')->orderBy('order_by', 'asc')->paginate(20);
        return view('backend.pages.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('category_name', 'id');


        return view('backend.pages.category.create', compact('categories'));
    }


    public function store(CategoryRequest $request)
    {
        $data = $request->all();
        $old_category = Category::latest()->first();
        if ($old_category) {
            $data['slug_id'] = $old_category->slug_id + 1;
        } else {
            $data['slug_id'] = 100000;
        }
        $data['slug'] = Str::slug($request->input('slug'), '-');

        if ($request->file('photo')) {
            $width = 800;
            $height = 800;
            $path = 'images/uploads/category/';
            $name = $data['slug'];
            $file = $request->file(('photo'));
            $data['photo'] = ImageUploadController::uploadImage($file, $width, $height, $name, $path);
        }
        if ($request->file('banner')) {
            $width = 1200;
            $height = 400;
            $path = 'images/uploads/category/';
            $name = $data['slug'] . '-banner';
            $file = $request->file(('banner'));
            $data['banner'] = ImageUploadController::uploadImage($file, $width, $height, $name, $path);
        }
        Category::create($data);
        return redirect(route('category.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $category->load('childCategory');
        // dd($category);
        return view('backend.pages.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::pluck('category_name', 'id');
       return  view('backend.pages.category.edit', compact('category','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $data = $request->all();
        $old_category = Category::latest()->first();
        if ($old_category) {
            $data['slug_id'] = $old_category->slug_id + 1;
        } else {
            $data['slug_id'] = 100000;
        }
        $data['slug'] = Str::slug($request->input('slug'), '-','en');

        if ($request->file('photo')) {
            $width = 800;
            $height = 800;
            $path = 'images/uploads/category/';
            $name = $data['slug'];
            $file = $request->file(('photo'));
            ImageUploadController::unlinkImage($path, $category->photo);
            $data['photo'] = ImageUploadController::uploadImage($file, $width, $height, $name, $path);

        }else{
            $data['photo'] = $category->photo;
        }
        if ($request->file('banner')) {
            $width = 1200;
            $height = 400;
            $path = 'images/uploads/category/';
            $name = $data['slug'] . '-banner';
            $file = $request->file(('banner'));
            ImageUploadController::unlinkImage($path, $category->banner);
            $data['banner'] = ImageUploadController::uploadImage($file, $width, $height, $name, $path);

        }else{
            $data['banner'] = $category->banner;
        }
        $category->update($data);
        return redirect(route('category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * destroy
     *
     * @param Category category
     *
     * @return void
     */

    public function destroy(Category $category)
    {
        $path = 'images/uploads/category/';
        ImageUploadController::unlinkImage($path, $category->photo);
        ImageUploadController::unlinkImage($path, $category->banner);
        $category->delete();
        return redirect(route('category.index'));
    }
}
