<?php

namespace App\Http\Controllers\Frontend;

use view;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    public function index()
    {
        return view('frontend.pages.index');
    }
    public function category()
    {
        return view('frontend.pages.category');
    }
    public function single()
    {
        return view('frontend.pages.single');
    }
}
